package com.example.proyecto.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.proyecto.entity.Entrenadores;

public interface EntrenadoresRepository extends JpaRepository<Entrenadores, Integer>{

    
} 