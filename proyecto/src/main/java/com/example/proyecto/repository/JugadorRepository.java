package com.example.proyecto.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.proyecto.entity.Jugador;

public interface JugadorRepository extends JpaRepository<Jugador, Integer>{

    
} 
