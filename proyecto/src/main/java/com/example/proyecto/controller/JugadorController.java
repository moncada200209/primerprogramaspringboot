package com.example.proyecto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;


import com.example.proyecto.entity.Jugador;
import com.example.proyecto.repository.JugadorRepository;

@Controller
public class JugadorController {

    @Autowired
    private JugadorRepository jugadorRepositorio;

    @GetMapping({"/verJugador","/mostrarJugador","/listarJugador"})  
	public String listarJugador(Model model) {
		List<Jugador> listaJugador = jugadorRepositorio.findAll();
		model.addAttribute("listaJugador", listaJugador);
		
		return "verJugador";
	}

	@GetMapping("/verJugador/formJugador")
    public String mostrarFormulario(Model model) {
        model.addAttribute("jugador", new Jugador()); // Asegurando que hay un objeto entrenador para el formulario
        return "formJugador"; // Asegúrate de tener formEntrenador.html en templates
    }

	@PostMapping("/guardarJugador")
    public String guardarClub(Jugador jugador, Model model) {     
        jugadorRepositorio.save(jugador);
        return "redirect:/verJugador";
    }

	@GetMapping("/jugador/editar/{id}")
	public String modificarClub (@PathVariable("id") Integer id, Model model) {
		Jugador jugador = jugadorRepositorio.findById(id).get();
		model.addAttribute("jugador", jugador);	
		
		return "formJugador";
	}

	@GetMapping("/jugador/eliminar/{id}")
	public String eliminarClub(@PathVariable("id") Integer id, Model model) {
		jugadorRepositorio.deleteById(id);
		return "redirect:/verJugador";
	}

}
