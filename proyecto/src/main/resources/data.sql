
INSERT INTO entrenadores (nombre, apellido, edad, nacionalidad) VALUES ('John', 'Doe', 45, 'American');
INSERT INTO entrenadores (nombre, apellido, edad, nacionalidad) VALUES ('Jane', 'Smith', 38, 'British');
-- INSERT INTO entrenadores (nombre, apellido, edad, nacionalidad) VALUES ('Carlos', 'Garcia', 50, 'Spanish');
-- INSERT INTO entrenadores (nombre, apellido, edad, nacionalidad) VALUES ('Anna', 'Ivanova', 34, 'Russian');


INSERT INTO asociacion (nombre, pais, presidente, siglas) VALUES ('FIFA', 'Switzerland', 'Gianni Infantino', 'FIFA');
INSERT INTO asociacion (nombre, pais, presidente, siglas) VALUES ('UEFA', 'Switzerland', 'Aleksander Čeferin', 'UEFA');
-- INSERT INTO asociacion (nombre, pais, presidente, siglas) VALUES ('CONMEBOL', 'Paraguay', 'Alejandro Domínguez', 'CONMEBOL');
-- INSERT INTO asociacion (nombre, pais, presidente, siglas) VALUES ('AFC', 'Malaysia', 'Shaikh Salman', 'AFC');